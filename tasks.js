'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  document.getElementById("deleteMe").remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wraperCollection = document.getElementsByClassName('wrapper');
  for (let wrap of wraperCollection) {
    const pColletion = wrap.getElementsByTagName('p');
    let sum = 0;
    while (pColletion.length > 0) {
      sum += Number(pColletion[0].textContent);
      pColletion[0].remove();
    }
    const newP = document.createElement('p');
    newP.textContent = sum;
    wrap.appendChild(newP);
  }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const changeMe = document.getElementById("changeMe");
  changeMe.type = "button";
  changeMe.value = "changed";
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const list = document.getElementById("changeChild");
  const li1 = document.createElement("li");
  li1.textContent = "1";
  list.insertBefore(li1, list.children[1]);
  const li3 = document.createElement("li");
  li3.textContent = "3";
  list.appendChild(li3);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const items = document.getElementsByClassName("item")
  for (let item of items) {
    item.classList.toggle("blue");
    item.classList.toggle("red");
  }
}

setTimeout(() => {
  removeBlock();
  calcParagraphs();
  changeInput();
  appendList();
  changeColors();
}, 1000);
